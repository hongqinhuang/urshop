﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Urs.Core;
using Urs.Core.Caching;
using Plugin.Api.Models.Common;
using Plugin.Api.Models.Coupons;
using Urs.Services.Stores;
using Urs.Services.Coupons;
using Urs.Services.Users;
using Urs.Services.Media;
using Urs.Framework;
using Urs.Framework.Controllers;

namespace Plugin.Api.Controllers
{
    /// <summary>
    /// 优惠券接口
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/coupon")]
    [ApiController]
    public class CouponController : BaseApiController
    {
        private readonly ICouponService _couponService;
        private readonly IUserService _userService;
        private readonly ICacheManager _cacheManager;
        private readonly IPictureService _pictureService;

        /// <summary>
        /// 构造器
        /// </summary>
        public CouponController(ICouponService couponService,
            IUserService userService,
            ICacheManager cacheManager,
            IPictureService pictureService)
        {
            this._couponService = couponService;
            this._userService = userService;
            this._cacheManager = cacheManager;
            this._pictureService = pictureService;
        }

        #region Util
        #endregion

        /// <summary>
        /// 获取优惠券列表[Auth]
        /// </summary>
        /// <param name="type">全部为：0， 过期：1，未过期：2</param>
        /// <param name="page">页码（默认：1）</param>
        /// <param name="size">页大小（默认值：12）</param>
        /// <returns></returns>
        [ApiAuthorize]
        [HttpGet("couponlist")]
        public async Task<ApiResponse<MoCouponList>> CouponList(int type, int page = 1, int size = 12)
        {
            var user = RegisterUser;
            var data = await Task.Run(() =>
              {
                  var model =new MoCouponList();
                  var command = new PagingFiltering
                  {
                      PageNumber = page,
                      PageSize = size > 0 ? size : 10
                  };
                  var list = _couponService.GetCouponsOrUser(user.Id, type, null, command.PageIndex, command.PageSize);
                  foreach (var i in list)
                  {
                      var coupon = new MoCoupon();
                      coupon.Id = i.Coupon.Id;
                      coupon.Title = i.Coupon.Title;
                      coupon.StartTime = i.Coupon.StartTime.ToString();
                      coupon.EndTime = i.Coupon.EndTime.ToString("yyyy-MM-dd");
                      coupon.Value = PriceFormatter.FormatPrice(i.Coupon.Value);
                      coupon.MinimumConsumption = i.Coupon.MinimumConsumption.ToString("F0");
                      model.Items.Add(coupon);
                  }
                  model.Paging.Page = command.PageNumber;
                  model.Paging.Size = command.PageSize;
                  return model;
              });
            return ApiResponse<MoCouponList>.Success(data);
        }

        /// <summary>
        /// 获取我的可用优惠券[Auth]
        /// </summary>
        /// <param name="orderTotal">订单购物车金额</param>
        /// <returns></returns>
        [ApiAuthorize]
        [HttpGet("usablecoupon")]
        public async Task<ApiResponse<List<MoCoupon>>> UsableCoupon(decimal orderTotal)
        {
            var user = RegisterUser;
            var data = await Task.Run(() =>
            {
                var list = new List<MoCoupon>();
                var coupon = _couponService.GetUsableCoupons(user.Id, orderTotal);
                foreach (var i in coupon)
                {
                    var model = new MoCoupon();
                    model.Id = i.Id;
                    model.Title = i.Title;
                    model.StartTime = i.StartTime.ToString();
                    model.EndTime = i.EndTime.ToString("yyyy-MM-dd");
                    model.Value = PriceFormatter.FormatPrice(i.Value);
                    model.MinimumConsumption = i.MinimumConsumption.ToString("F0");
                    list.Add(model);
                }

                return list;
            });
            return ApiResponse<List<MoCoupon>>.Success(data);
        }

    }
}
