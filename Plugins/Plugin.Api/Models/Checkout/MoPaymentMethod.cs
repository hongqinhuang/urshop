﻿using System.Collections.Generic;

namespace Plugin.Api.Models.Checkout
{
    public partial class MoPaymentMethod
    {
        /// <summary>
        /// 支付系统名称
        /// </summary>
        public string PaymentMethodSystemName { get; set; }
        /// <summary>
        /// 支付名称
        /// </summary>
        public string Name { get; set; }
    }
}