﻿using System.Collections.Generic;
using Plugin.Api.Models.Common;
using Plugin.Api.Models.Goods;

namespace Plugin.Api.Models.Favorites
{
    /// <summary>
    /// 收藏夹列表
    /// </summary>
    public partial class MoFavoritesList
    {
        public MoFavoritesList()
        {
            Items = new List<MoGoodsOverview>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 评论Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 收藏商品
        /// </summary>
        public IList<MoGoodsOverview> Items { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }
        /// <summary>
        /// 评论时间
        /// </summary>
        public string CreateTime { get; set; }
    }
}