﻿using System;
using System.Collections.Generic;
using Plugin.Api.Models.Common;
using Plugin.Api.Models.UserAddress;
using Plugin.Api.Models.Media;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Plugin.Api.Models.Order
{
    /// <summary>
    /// 订单详情
    /// </summary>
    public partial class MoOrderDetail : BaseEntityModel
    {
        public MoOrderDetail()
        {
            Items = new List<OrderGoods>();
            Notes = new List<OrderNote>();
            Shipments = new List<ShipmentBriefModel>();
            BillingAddress = new MoBillingAddress();
            ShippingAddress = new MoAddress();
        }
        /// <summary>
        /// 订单编号
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 订单Guid
        /// </summary>
        public string OrderGuid { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 支付时间
        /// </summary>
        public DateTime? PaidDateUtc { get; set; }
        /// <summary>
        /// 配送时间
        /// </summary>
        public DateTime? ShippedDateUtc { get; set; }
        /// <summary>
        /// 确认收货时间
        /// </summary>
        public DateTime? DeliveryDateUtc { get; set; }
        /// <summary>
        /// 评论时间
        /// </summary>
        public DateTime? ReviewDateUtc { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public string OrderStatus { get; set; }
        /// <summary>
        /// 订单状态Id
        /// </summary>
        public int OrderStatusId { get; set; }
        /// <summary>
        /// 是否允许再支付
        /// </summary>
        public bool IsPendingAllowed { get; set; }
        /// <summary>
        /// 是否允许退换货
        /// </summary>
        public bool IsReturn { get; set; }
        /// <summary>
        /// 是否存在配送
        /// </summary>
        public bool IsShippable { get; set; }
        /// <summary>
        /// 配送状态
        /// </summary>
        public string ShippingStatus { get; set; }
        /// <summary>
        /// 配送状态Id
        /// </summary>
        public int ShippingStatusId { get; set; }
        /// <summary>
        /// 配送地址
        /// </summary>
        public MoAddress ShippingAddress { get; set; }
        /// <summary>
        /// 配送方式
        /// </summary>
        public string ShippingMethod { get; set; }
        /// <summary>
        /// 配送项
        /// </summary>
        public IList<ShipmentBriefModel> Shipments { get; set; }
        /// <summary>
        /// 发票项
        /// </summary>
        public MoBillingAddress BillingAddress { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        public string PaymentMethod { get; set; }
        /// <summary>
        /// 商品项金额
        /// </summary>
        public string OrderSubtotal { get; set; }
        /// <summary>
        /// 配送金额
        /// </summary>
        public string OrderShipping { get; set; }
        /// <summary>
        /// 支付额外支付金额
        /// </summary>
        public string PaymentMethodAdditionalFee { get; set; }
        /// <summary>
        /// 结算属性
        /// </summary>
        public string CheckoutAttributeInfo { get; set; }
        /// <summary>
        /// 返回积分
        /// </summary>
        public decimal RedeemedRewardPoints { get; set; }
        /// <summary>
        /// 抵扣积分
        /// </summary>
        public string RedeemedRewardPointsAmount { get; set; }
        /// <summary>
        /// 兑换积分金额
        /// </summary>
        public string ExchangePointsAmount { get; set; }
        /// <summary>
        /// 订单总金额
        /// </summary>
        public string OrderTotal { get; set; }
        /// <summary>
        /// 订单商品项
        /// </summary>
        public IList<OrderGoods> Items { get; set; }
        /// <summary>
        /// 订单跟踪
        /// </summary>
        public IList<OrderNote> Notes { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        #region Nested Classes
        /// <summary>
        /// 订单商品
        /// </summary>
        public partial class OrderGoods : BaseEntityModel
        {
            /// <summary>
            /// 商品Sku
            /// </summary>
            public string Sku { get; set; }
            /// <summary>
            /// 商品Id
            /// </summary>
            public int GoodsId { get; set; }
            /// <summary>
            /// 商品名
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 商品SeName(Url)
            /// </summary>
            public string SeName { get; set; }
            /// <summary>
            /// 商品单价
            /// </summary>
            public string UnitPrice { get; set; }
            /// <summary>
            /// 商品小计
            /// </summary>
            public string SubTotal { get; set; }
            /// <summary>
            /// 商品数量
            /// </summary>
            public int Quantity { get; set; }
            /// <summary>
            /// 商品属性
            /// </summary>
            public string AttributeInfo { get; set; }
            /// <summary>
            /// 商品图
            /// </summary>
            public MoPicture Picture { get; set; }
        }
        /// <summary>
        /// 订单备注
        /// </summary>
        public partial class OrderNote : BaseModel
        {
            /// <summary>
            /// 订单注释
            /// </summary>
            public string Note { get; set; }
            /// <summary>
            /// 创建时间
            /// </summary>
            public DateTime Time { get; set; }
        }
        /// <summary>
        /// 配送信息
        /// </summary>
        public partial class ShipmentBriefModel : BaseEntityModel
        {
            /// <summary>
            /// 快递公司
            /// </summary>
            public string ExpressName { get; set; }
            /// <summary>
            /// 快递编号
            /// </summary>
            public string TrackingNumber { get; set; }
            /// <summary>
            /// 发货时间
            /// </summary>
            public DateTime? ShippedDate { get; set; }
            /// <summary>
            /// 收货时间
            /// </summary>
            public DateTime? DeliveryDate { get; set; }
        }
        #endregion
    }
}