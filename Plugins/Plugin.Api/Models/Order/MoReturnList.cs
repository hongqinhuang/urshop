﻿using System;
using System.Collections.Generic;
using Plugin.Api.Models.Media;

namespace Plugin.Api.Models.Order
{
    /// <summary>
    /// 退换货
    /// </summary>
    public partial class MoReturnList
    {
        public MoReturnList()
        {
            Items = new List<AfterSales>();
        }
        /// <summary>
        /// 退货项
        /// </summary>
        public IList<AfterSales> Items { get; set; }

        #region Nested classes
        public partial class AfterSales
        {
            public int Id { get; set; }
            /// <summary>
            /// 退换货状态
            /// </summary>
            public string Status { get; set; }
            /// <summary>
            /// 商品Id
            /// </summary>
            public int GoodsId { get; set; }
            /// <summary>
            /// 商品名称
            /// </summary>
            public string GoodsName { get; set; }
            /// <summary>
            /// 商品url
            /// </summary>
            public string GoodsSeName { get; set; }
            /// <summary>
            /// 退货数量
            /// </summary>
            public int Qty { get; set; }
            /// <summary>
            /// 退货原因
            /// </summary>
            public string Reason { get; set; }
            /// <summary>
            /// 退货操作
            /// </summary>
            public string Action { get; set; }
            /// <summary>
            /// 说明
            /// </summary>
            public string Comments { get; set; }
            /// <summary>
            /// 图片
            /// </summary>
            public MoPicture Picture { get; set; }
            /// <summary>
            /// 创建时间
            /// </summary>
            public DateTime CreateTime { get; set; }
        }
        #endregion
    }
}