﻿using System.Collections.Generic;
using Plugin.Api.Models.Common;
using Urs.Framework.Mvc;

namespace Plugin.Api.Models.Topics
{
    /// <summary>
    /// 单页列表
    /// </summary>
    public partial class MoTopicList
    {
        public MoTopicList()
        {
            Topics = new List<MoTopic>();
            Paging = new MoPaging();
        }
        /// <summary>
        /// 单页信息
        /// </summary>
        public IList<MoTopic> Topics { get; set; }
        /// <summary>
        /// 分页
        /// </summary>
        public MoPaging Paging { get; set; }
    }
}