using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using Urs.Core.Domain.Customers;
using Urs.Services.Authentication.External;
using Urs.Services.Logging;

namespace Urs.Plugin.ExternalAuth.WeixinOpen.Core
{
    public class WeixinOpenProviderAuthorizer : IWeixinOpenProviderAuthorizer
    {
        private readonly IExternalAuthorizer _authorizer;
        private readonly WeixinOpenExternalAuthSettings _weixinOpenExternalAuthSettings;
        private readonly ExternalAuthenticationSettings _externalAuthenticationSettings;
        private readonly ILogger _logger;

        public WeixinOpenProviderAuthorizer(
            IExternalAuthorizer authorizer,
            ExternalAuthenticationSettings externalAuthenticationSettings,
            WeixinOpenExternalAuthSettings weixinOpenExternalAuthSettings,
            ILogger logger)
        {
            this._authorizer = authorizer;
            this._externalAuthenticationSettings = externalAuthenticationSettings;
            this._weixinOpenExternalAuthSettings = weixinOpenExternalAuthSettings;
            this._logger = logger;
        }

        public AuthorizeState Authorize(string code, Customer customer, bool isApi)
        {

            var url = "https://api.weixin.qq.com/sns/jscode2session";

            RestClient client = new RestClient(url);

            RestRequestExt request = new RestRequestExt();
            request.AddParameter("appid", _weixinOpenExternalAuthSettings.AppId);
            request.AddParameter("secret", _weixinOpenExternalAuthSettings.AppSecret);
            request.AddParameter("js_code", code);
            request.AddParameter("grant_type", "authorization_code");

            try
            {
                var response = client.Execute(request);
                _logger.Error(response.Content);
                JObject codeJson = JObject.Parse(response.Content);

                //����
                if (codeJson["errcode"] != null)
                {
                    var errcode = codeJson["errcode"].ToString();
                    var state = new AuthorizeState(string.Empty, OpenAuthenticationStatus.Error);
                    state.AddError(errcode);
                    return state;
                }
                string unionid = string.Empty;
                //if (codeJson["unionid"] != null)
                //    unionid = codeJson["unionid"].ToString();
                string openId = codeJson["openid"].ToString();
                string session_key = codeJson["session_key"].ToString();
                string ticket = string.Empty;
                var parameters = new OAuthAuthParameters(Provider.SystemName);
                parameters.OpenId = openId;
                parameters.OAuthToken = session_key;

                if (_externalAuthenticationSettings.AutoRegisterEnabled)
                    GetClaims(parameters);
                
                var result = _authorizer.ApiAuthorize(parameters, customer);
                result.OpenId = openId;
                return new AuthorizeState(code, result);

            }
            catch (Exception ex)
            {
                var state = new AuthorizeState(string.Empty, OpenAuthenticationStatus.AssociateOnLogon);
                state.AddError(ex.Message);
                return state;
            }
        }

        private void GetClaims(OAuthAuthParameters parameters)
        {
            RestClient clientUser = new RestClient("https://api.weixin.qq.com/sns/userinfo");
            RestRequestExt requestUser = new RestRequestExt();
            requestUser.AddParameter("access_token", parameters.OAuthToken);
            requestUser.AddParameter("openid", parameters.OpenId);
            requestUser.AddParameter("lang", "zh_CN");

            var responseUser = clientUser.Execute(requestUser);
            JObject userInfo = JObject.Parse(responseUser.Content);

            if (userInfo["errcode"] == null)
            {
                //parameters.ExternalDisplayIdentifier = userInfo["unionid"].ToString();
                var claimsTranslator = new WeixinOpenClaimsTranslator();
                parameters.UserClaim = claimsTranslator.Translate(userInfo);
            }
            else
            {
                _logger.Error(userInfo["errcode"].ToString());
            }
        }
    }

    public class RestRequestExt : RestRequest
    {
        public new IRestRequest AddParameter(string name, object value)
        {
            if (value == null)
                value = string.Empty;
            return base.AddParameter(name, value);
        }
    }
}