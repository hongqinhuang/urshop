﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Urs.Framework.Mvc.Routes;

namespace ExternalAuth.WeixinOpen.Infrastructure
{
    public partial class RouteProvider : IRouteProvider
    {
        /// <summary>
        /// Register routes
        /// </summary>
        /// <param name="routeBuilder">Route builder</param>
        public void RegisterRoutes(IRouteBuilder routes)
        {

            routes.MapRoute("Plugin.ExternalAuth.WeixinOpen.Configure",
               "plugins/weixinopen/configure",
               new { controller = "ExternalAuthWeixinOpen", action = "Configure" });

            routes.MapRoute("Plugin.ExternalAuth.WeixinOpen.PublicInfo",
                 "plugins/weixinopen/publicInfo",
                 new { controller = "ExternalAuthWeixinOpen", action = "PublicInfo" });

            routes.MapRoute("Plugin.ExternalAuth.WeixinOpen.GetPhoneNumber",
                "plugins/weixinopen/getphonenumber",
                 new { controller = "ExternalAuthWeixinOpen", action = "GetPhoneNumber" });
            
            routes.MapRoute("Plugin.ExternalAuth.WeixinOpen.LoginApi",
                "plugins/weixinopen/loginapi",
                 new { controller = "ExternalAuthWeixinOpen", action = "LoginApi" });

        }

        /// <summary>
        /// Gets a priority of route provider
        /// </summary>
        public int Priority
        {
            get { return 0; }
        }
    }
}