﻿using System;
using Urs.Framework.Mvc;

namespace Urs.Plugin.Payments.WeixinOpen.Models
{
    public class PayingInfoModel : BaseModel
    {
        public int OrderId { get; set; }
        public string OrderTotal { get; set; }
        public DateTime CreateTime { get; set; }
        public string wxJsApiParam { get; set; }
    }
}