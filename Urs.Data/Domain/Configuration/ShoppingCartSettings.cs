﻿
using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class ShoppingCartSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a value indicating whether a custoemr should be redirected to the shopping cart page after adding a goods to the cart/wishlist
        /// </summary>
        public bool DisplayCartAfterAddingGoods { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a custoemr should be redirected to the shopping cart page after adding a goods to the cart/wishlist
        /// </summary>
        public bool DisplayWishlistAfterAddingGoods { get; set; }

        public int MinimunShoppingCartItemGoodsQuantity { get; set; }
        /// <summary>
        /// Gets or sets a value indicating maximum number of items in the shopping cart
        /// </summary>
        public int MaximumShoppingCartItems { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating maximum number of items in the wishlist
        /// </summary>
        public int MaximumWishlistItems { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show goods images in the mini-shopping cart block
        /// </summary>
        public bool AllowOutOfStockItemsToBeAddedToWishlist { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to move items from wishlist to cart when clicking "Add to cart" button. Otherwise, they are copied.
        /// </summary>
        public bool MoveItemsFromWishlistToCart { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show goods image on shopping cart page
        /// </summary>
        public bool ShowGoodsImagesOnShoppingCart { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show goods image on wishlist page
        /// </summary>
        public bool ShowGoodsImagesOnWishList { get; set; }

        /// <summary>
        /// Gets or sets a number of "Cross-sells" on shopping cart page
        /// </summary>
        public int CrossSellsNumber { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether "email a wishlist" feature is enabled
        /// </summary>
        public bool EmailWishlistEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to enabled "email a wishlist" for anonymous users.
        /// </summary>
        public bool AllowAnonymousUsersToEmailWishlist { get; set; }
        
        /// <summary>Gets or sets a value indicating whether mini-shopping cart is enabled
        /// </summary>
        public bool MiniShoppingCartEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show goods images in the mini-shopping cart block
        /// </summary>
        public bool ShowGoodsImagesInMiniShoppingCart { get; set; }

        /// <summary>Gets or sets a maximum number of goods which can be displayed in the mini-shopping cart block
        /// </summary>
        public int MiniShoppingCartGoodsNumber { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether to round calculated prices and total during calculation
        /// </summary>
        public bool RoundPricesDuringCalculation { get; set; }
    }
}