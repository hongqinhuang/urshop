using Urs.Core;
using System.Collections.Generic;
using Urs.Data.Domain.Localization;

namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// 商品标签
    /// </summary>
    public partial class GoodsTag : BaseEntity
    {
        private ICollection<GoodsTagMapping> _goods;

        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public virtual int Count { get; set; }

        public virtual ICollection<GoodsTagMapping> Goods
        {
            get { return _goods ?? (_goods = new List<GoodsTagMapping>()); }
            protected set { _goods = value; }
        }
    }
}
