﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Common;

namespace Urs.Data.Mapping.Common
{
    public partial class AddressMap : UrsEntityTypeConfiguration<Address>
    {
        public override void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.ToTable(nameof(Address));
            builder.HasKey(a => a.Id);
            base.Configure(builder);
        }
    }
}
