﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Common;

namespace Urs.Data.Mapping.Common
{
    public partial class FavoritesMap : UrsEntityTypeConfiguration<Favorites>
    {
        public override void Configure(EntityTypeBuilder<Favorites> builder)
        {
            builder.ToTable(nameof(Favorites));
            builder.HasKey(a => a.Id);
            base.Configure(builder);
        }
    }
}
