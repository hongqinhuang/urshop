﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Logging;

namespace Urs.Data.Mapping.Logging
{
    public partial class ActivityLogTypeMap : UrsEntityTypeConfiguration<ActivityLogType>
    {
        public override void Configure(EntityTypeBuilder<ActivityLogType> builder)
        {
            builder.ToTable(nameof(ActivityLogType));
            builder.HasKey(alt => alt.Id);

            builder.Property(alt => alt.SystemKeyword).IsRequired().HasMaxLength(100);
            builder.Property(alt => alt.Name).IsRequired().HasMaxLength(200);

            base.Configure(builder);
        }
    }
}
