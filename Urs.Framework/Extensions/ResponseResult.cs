﻿using System.Collections;

namespace Urs.Framework.Kendoui
{
    public class ResponseResult
    {
        public IEnumerable data { get; set; }
        public object msg { get; set; }
        public int code { get; set; }
        public int count { get; set; }
    }
}
