﻿using Urs.Core;
using Urs.Core.Infrastructure;
using Urs.Services.Localization;
using Urs.Framework.Mvc;

namespace Urs.Framework
{
    public class UrsDisplayName : System.ComponentModel.DisplayNameAttribute
    {
        private string _resourceValue = string.Empty;

        public UrsDisplayName(string resourceKey)
            : base(resourceKey)
        {
            ResourceKey = resourceKey;
        }

        public string ResourceKey { get; set; }

        public override string DisplayName
        {
            get
            {
                    _resourceValue = EngineContext.Current
                        .Resolve<ILocalizationService>()
                        .GetResource(ResourceKey, true, ResourceKey);
                return _resourceValue;
            }
        }

        public string Name
        {
            get { return "UrsDisplayName"; }
        }
    }
}
