﻿using System;
using Urs.Services.Tasks;
using Urs.Services.Orders;

namespace Urs.Services.Shipping
{
    public partial class DeliverShipmentTask : IScheduleTask
    {
        private readonly IOrderProcessingService _orderProcessingService;

        public DeliverShipmentTask(IOrderProcessingService orderProcessingService)
        {
            this._orderProcessingService = orderProcessingService;
        }

        public void Execute()
        {
            _orderProcessingService.ConfirmDeliver();
        }
    }
}
