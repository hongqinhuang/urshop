using System.Collections.Generic;
using Urs.Data.Domain.Stores;

namespace Urs.Services.Stores
{
    public partial interface IGoodsSpecService
    {
        #region Goods spec

        void DeleteGoodsSpec(GoodsSpec goodsSpec);

        IList<GoodsSpec> GetAllGoodsSpecs(string name = null);

        GoodsSpec GetGoodsSpecById(int goodsSpecId);

        void InsertGoodsSpec(GoodsSpec goodsSpec);

        void UpdateGoodsSpec(GoodsSpec goodsSpec);

        #endregion

        #region Goods spec mappings

        void DeleteGoodsSpecMapping(GoodsSpecMapping goodsSpec);

        IList<GoodsSpecMapping> GetGoodsSpecMappingByGoodsId(int goodsId);

        GoodsSpecMapping GetGoodsSpecMappingById(int goodsSpecId);

        GoodsSpecMapping GetGoodsSpecMapping(int goodsId, int goodsSpecValueId);
        void InsertGoodsSpecMapping(GoodsSpecMapping goodsSpec);

        void UpdateGoodsSpecMapping(GoodsSpecMapping goodsSpec);

        #endregion

        #region Goods spec values (goodsSpecValue)

        void DeleteGoodsSpecValue(GoodsSpecValue goodsSpecValue);

        IList<GoodsSpecValue> GetGoodsSpecValues(int goodsSpecId);

        GoodsSpecValue GetGoodsSpecValueById(int goodsSpecValueId);

        void InsertGoodsSpecValue(GoodsSpecValue goodsSpecValue);

        void UpdateGoodsSpecValue(GoodsSpecValue goodsSpecValue);

        #endregion

        #region Goods spec compinations

        void DeleteGoodsSpecCombination(GoodsSpecCombination combination);

        IList<GoodsSpecCombination> GetAllGoodsSpecCombinations(int goodsId);

        GoodsSpecCombination GetGoodsSpecCombinationById(int goodsSpecCombinationId);

        void InsertGoodsSpecCombination(GoodsSpecCombination combination);

        void UpdateGoodsSpecCombination(GoodsSpecCombination combination);

        #endregion
    }
}
