﻿using System;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Agents
{
    public partial class AgentUsersModel : BaseEntityModel
    {
        public int UserId { get; set; }
        public string Nickname { get; set; }
        public string ShopName { get; set; }
        public string Phone { get; set; }
        public decimal Rate { get; set; }
        public decimal ParentRate { get; set; }
        public string ImgUrl { get; set; }
        public DateTime CreateTime { get; set; }
    }
}