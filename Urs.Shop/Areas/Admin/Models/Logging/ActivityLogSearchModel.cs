using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Urs.Framework;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Logging
{
    public partial class ActivityLogSearchModel : BaseModel
    {
        public ActivityLogSearchModel()
        {
            ActivityLogType = new List<SelectListItem>();
        }
        [UrsDisplayName("Admin.Configuration.ActivityLog.ActivityLog.Fields.CreateTimeFrom")]
        [UIHint("DateNullable")]
        public DateTime? CreateTimeFrom { get; set; }

        [UrsDisplayName("Admin.Configuration.ActivityLog.ActivityLog.Fields.CreateTimeTo")]
        [UIHint("DateNullable")]
        public DateTime? CreateTimeTo { get; set; }

        [UrsDisplayName("Admin.Configuration.ActivityLog.ActivityLog.Fields.ActivityLogType")]
        public int ActivityLogTypeId { get; set; }

        [UrsDisplayName("Admin.Configuration.ActivityLog.ActivityLog.Fields.ActivityLogType")]
        public IList<SelectListItem> ActivityLogType { get; set; }
    }
}