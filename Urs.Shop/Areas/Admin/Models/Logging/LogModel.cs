﻿using System;
using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Logging
{
    public partial class LogModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.System.Log.Fields.LogLevel")]
        public string LogLevel { get; set; }

        [UrsDisplayName("Admin.System.Log.Fields.ShortMessage")]
        
        public string ShortMessage { get; set; }

        [UrsDisplayName("Admin.System.Log.Fields.FullMessage")]
        
        public string FullMessage { get; set; }

        [UrsDisplayName("Admin.System.Log.Fields.IPAddress")]
        
        public string IpAddress { get; set; }

        [UrsDisplayName("Admin.System.Log.Fields.User")]
        public int? UserId { get; set; }
        [UrsDisplayName("Admin.System.Log.Fields.User")]
        public string UserEmail { get; set; }

        [UrsDisplayName("Admin.System.Log.Fields.PageURL")]
        
        public string PageUrl { get; set; }

        [UrsDisplayName("Admin.System.Log.Fields.ReferrerURL")]
        
        public string ReferrerUrl { get; set; }

        [UrsDisplayName("Admin.System.Log.Fields.CreateTime")]
        public DateTime CreateTime { get; set; }
    }
}