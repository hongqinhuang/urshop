﻿using Urs.Admin.Models.Common;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Orders
{
    public partial class OrderAddressModel : BaseModel
    {

        public int OrderId { get; set; }

        public AddressModel Address { get; set; }
    }
}