﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Settings
{
    public partial class CommonSettingsModel : BaseModel, ISettingsModel
    {
        public CommonSettingsModel()
        {
            StoreInformationSettings = new StoreInformationSettingsModel();
            SecuritySettings = new SecuritySettingsModel();
            PdfSettings = new PdfSettingsModel();
        }
        public StoreInformationSettingsModel StoreInformationSettings { get; set; }
        public SecuritySettingsModel SecuritySettings { get; set; }
        public PdfSettingsModel PdfSettings { get; set; }

        #region Nested classes

        public partial class StoreInformationSettingsModel : BaseModel
        {
            public StoreInformationSettingsModel()
            {
                this.AvailableStoreThemesForDesktops = new List<SelectListItem>();
                this.AvailableStoreThemesForMobileDevices = new List<SelectListItem>();
            }
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.StoreName")]
            
            public string StoreName { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.StoreUrl")]
            
            public string StoreUrl { get; set; }
            [UIHint("Picture")]
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.StoreLogo1")]
            public int StoreLogo1 { get; set; }
            [UIHint("Picture")]
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.StoreLogo2")]
            public int StoreLogo2 { get; set; }
            [UIHint("Picture")]
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.StoreLogo3")]
            public int StoreLogo3 { get; set; }
            /// <summary>
            /// 版权信息
            /// </summary>
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CopyrightInfo")]
            public string CopyrightInfo { get; set; }
            /// <summary>
            /// 备案号
            /// </summary>
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.ICPRecord")]
            public string ICPRecord { get; set; }
            /// <summary>
            /// 网站服务时间
            /// </summary>
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.StoreServiceTime")]
            public string StoreServiceTime { get; set; }
            /// <summary>
            /// 网站服务电话
            /// </summary>
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.ServiceTelePhone")]
            public string ServiceTelePhone { get; set; }
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.OnlineConsultingCode")]
            public string OnlineConsultingCode { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.MobileDevicesSupported")]
            public bool MobileDevicesSupported { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.StoreClosed")]
            public bool StoreClosed { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.StoreClosedAllowForAdmins")]
            public bool StoreClosedAllowForAdmins { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.DefaultStoreThemeForDesktops")]
            
            public string DefaultStoreThemeForDesktops { get; set; }
            public IList<SelectListItem> AvailableStoreThemesForDesktops { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.DefaultStoreThemeForMobileDevices")]
            
            public string DefaultStoreThemeForMobileDevices { get; set; }
            public IList<SelectListItem> AvailableStoreThemesForMobileDevices { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.AllowUserToSelectTheme")]
            public bool AllowUserToSelectTheme { get; set; }

        }

        public partial class SeoSettingsModel : BaseModel
        {
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.PageTitleSeparator")]
            
            public string PageTitleSeparator { get; set; }
            public SelectList PageTitleSeoAdjustmentValues { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.DefaultTitle")]
            
            public string DefaultTitle { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.DefaultMetaKeywords")]
            
            public string DefaultMetaKeywords { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.DefaultMetaDescription")]
            
            public string DefaultMetaDescription { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.GenerateGoodsMetaDescription")]
            
            public bool GenerateGoodsMetaDescription { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.ConvertNonWesternChars")]
            public bool ConvertNonWesternChars { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CanonicalUrlsEnabled")]
            public bool CanonicalUrlsEnabled { get; set; }
        }

        public partial class SecuritySettingsModel : BaseModel
        {
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.EncryptionKey")]
            
            public string EncryptionKey { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.AdminAreaAllowedIpAddresses")]
            
            public string AdminAreaAllowedIpAddresses { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.HideAdminMenuItemsBasedOnPermissions")]
            public bool HideAdminMenuItemsBasedOnPermissions { get; set; }




            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CaptchaEnabled")]
            public bool CaptchaEnabled { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CaptchaShowOnLoginPage")]
            public bool CaptchaShowOnLoginPage { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CaptchaShowOnRegistrationPage")]
            public bool CaptchaShowOnRegistrationPage { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CaptchaShowOnContactUsPage")]
            public bool CaptchaShowOnContactUsPage { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CaptchaShowOnEmailWishlistToFriendPage")]
            public bool CaptchaShowOnEmailWishlistToFriendPage { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CaptchaShowOnEmailGoodsToFriendPage")]
            public bool CaptchaShowOnEmailGoodsToFriendPage { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CaptchaShowOnBlogCommentPage")]
            public bool CaptchaShowOnBlogCommentPage { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CaptchaShowOnNewsCommentPage")]
            public bool CaptchaShowOnNewsCommentPage { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.CaptchaShowOnGoodsReviewPage")]
            public bool CaptchaShowOnGoodsReviewPage { get; set; }
            
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.reCaptchaPublicKey")]
            
            public string ReCaptchaPublicKey { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.reCaptchaPrivateKey")]
            
            public string ReCaptchaPrivateKey { get; set; }




            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.UseSSL")]
            public bool UseSsl { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.SharedSSLUrl")]
            
            public string SharedSslUrl { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.NonSharedSSLUrl")]
            
            public string NonSharedSslUrl { get; set; }
        }

        public partial class PdfSettingsModel : BaseModel
        {
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.PdfEnabled")]
            public bool Enabled { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.PdfLetterPageSizeEnabled")]
            public bool LetterPageSizeEnabled { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.PdfLogo")]
            [UIHint("Picture")]
            public int LogoPictureId { get; set; }
        }

        public partial class LocalizationSettingsModel : BaseModel
        {
            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.UseImagesForLanguageSelection")]
            public bool UseImagesForLanguageSelection { get; set; }

            [UrsDisplayName("Admin.Configuration.Settings.GeneralCommon.SeoFriendlyUrlsForLanguagesEnabled")]
            public bool SeoFriendlyUrlsForLanguagesEnabled { get; set; }
        }

        #endregion
    }
}