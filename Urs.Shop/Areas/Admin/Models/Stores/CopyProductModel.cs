﻿using Urs.Framework.Models;

namespace Urs.Admin.Models.Stores
{
    public partial class CopyGoodsModel : BaseEntityModel
    {
        public int GoodsId { get; set; }
        public string Name { get; set; }
        public bool CopyImages { get; set; }
        public bool Published { get; set; }

    }
}