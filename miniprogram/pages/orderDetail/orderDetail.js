import api from '../../api/api'
import {
  orderDetail
} from '../../api/conf'

Page({
  data: {
    select: 0,
    group: [],
    catId: 0,
    subcateProduct: [],
    page: 1,
    subid: 0,
    isHome: true,
    proNum: 1,
    isSelect: [],
    isSelectIndex: [],
    isSelectValue: '',
    productArr: [],
    customerId: 0
  },
  onLoad: function (options) {
    this.setData(options)
    this.getOrderDetail(options.oid)
  },
  onReady: function () {

  },
  onShow: function () {
   
  },
  getOrderDetail(oid){
var that=this;
    api.get(orderDetail, {
      oid: oid
    }).then((res) => {
      that.setData({
        orderDetail:res.Data
      })
    })
  }
})