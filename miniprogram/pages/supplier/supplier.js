import api from '../../api/api'
import { formsubmit } from '../../api/conf'
Page({

  data: {
    
  },
  onLoad: function (options) {
    
  },
  onReady: function () {
    
  },
  onShow: function () {
    
  },
  formSubmit: function (e) {
    let phone = parseInt(e.detail.value.Phone)
    if (!(/^1[34578]\d{9}$/.test(phone))) {
      wx.showToast({
        title: '手机号格式有误',
        icon: 'none'
      })
      return
    }
    var that = this
    wx.showLoading({
      title: '提交中',
      mask: true
    })
    api.post(formsubmit +'?name=Vendor', {
      Name: e.detail.value.Name,
      Phone: e.detail.value.Phone,
      Form:[{
        Key: 17,
        Value: e.detail.value.textarea
      }]
    }).then(res => {
      wx.hideLoading()
      wx.showToast({
        title: '提交成功',
      })
      setTimeout(function () {
        wx.switchTab({
          url: '/pages/my/my',
        })
      }, 1500)
    }).catch(err => {

    })
  }
})